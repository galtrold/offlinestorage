﻿
var gulp = require("gulp");
var handlebars = require('gulp-handlebars');
var concat = require('gulp-concat');
var declare = require('gulp-declare');
var watch = require('gulp-watch');

gulp.task('templates', function () {
    gulp.src(['Scripts/App/**/*.handlebars'])
        .pipe(handlebars())
        .pipe(declare({
            namespace: 'Handlebars.templates'
        }))
        .pipe(concat("templates.js"))
        .pipe(gulp.dest('Scripts/App/templates/'));
});

gulp.task("watch", function () {
    gulp.watch("Scripts/App/**/*.handlebars", ["templates"]);
});
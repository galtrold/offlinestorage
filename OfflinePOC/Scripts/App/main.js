﻿/**
 * Created by cjh on 17-02-14.
 */
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () {
};


Vivi = (function (Backbone, Marionette) {

    var app = new Backbone.Marionette.Application();

    app.localStorageEnabled = true;

    app.addInitializer(function () {
        


    });

    app.addRegions({
        header: "#header",
        main: "#main"
    });

    app.on("initialize:after", function (options) {

        if (Backbone.history) {
            Backbone.history.start();



        }
    });

    return app;

})(Backbone, Marionette);


// Boot application
$(function () {
    Vivi.start();
});



// Deprecated since version 0.8.0 
Handlebars.registerHelper("formatDate", function(datetime, format) {
    var date = new Date(datetime);
    return date.toUTCString();

});
 
﻿Vivi.module("forside", function(Forside, App, Backbone, Marionette, $, _) {

    Forside.Controller = Marionette.Controller.extend({

        initialize: function(options) {

            _.bindAll(this, "addBesoeg");

            this.counter = 0;

            this.besoegsCollection = new App.Entities.BesoegsCollection();
            this.besoegsCollection.fetch();

            // Load view
            this.forsideView = new Forside.ForsideView({ collection: this.besoegsCollection });
            this.listenTo(this.forsideView, "addbesoeg", this.addBesoeg);
            this.listenTo(this.forsideView, "itemview:deleteBesoeg", this.deleteBesoeg);
            
            this.listenTo(this.forsideView, "sync", this.sync);


            // Show page
            App.main.show(this.forsideView);

        },
        addBesoeg: function(arg) {
            this.counter++;
            var date = new Date();
            var besoeg = this.besoegsCollection.add({ navn: "Bilka, Ishøj", kontaktperson: this.counter + ": Per Larsen", dato: date.toJSON() });
            besoeg.save();


        },
        deleteBesoeg: function (args) {

            args.model.destroy();
            
        },
        sync: function (arg) {
            App.Entities.Model.flushGarbage();

            App.localStorageEnabled = false;
            this.besoegsCollection.each(function(item) {
                item.save();
            });
            App.localStorageEnabled = true;
        }

    });

});
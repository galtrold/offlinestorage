﻿Vivi.module("forside", function (forside, app, Backbone, Marionette, $, _) {
    
    forside.BesoegsItem = Marionette.ItemView.extend({
        tagName: "div",
        className: "row",
        triggers: {
            "click [data-role='deleteBesoeg']": "deleteBesoeg",
        },
        initialize: function(options) {
            this.edit = false;
        },
        getTemplate: function () {
            return this.edit ? "editBesoegItemView" : "besogesItemView";
        },
        events: {
            "click [data-role='editBesoeg']": "toggleEdit",
            
            "change input": "modelChanged"
        },
        
        toggleEdit: function(event) {
            this.edit = !this.edit;
            this.render();
        },
        modelChanged: function(event) {
            this.model.set(event.target.id, event.target.value);
            this.model.save();
        }
    });

    forside.ForsideView = Marionette.CompositeView.extend({
        tagName: "div",
        template: "forside",
        itemView: forside.BesoegsItem,
        itemViewContainer: "#besoegsListe",
        triggers: {
            "click .addBesoegBtn": "addbesoeg",
            "click .btn-info" : "sync"
        },
        
    });

});
﻿Vivi.module("forside", function (forside, app, Backbone, Marionette, $, _) {

    forside.Router = Marionette.AppRouter.extend({
        appRoutes: {
            "": "showView"
        }
    });

    var API = {
        showView: function () {
            var controller = new forside.Controller();
        }
    }

    app.addInitializer(function () {
        var router = new forside.Router({ controller: API });
    });

});
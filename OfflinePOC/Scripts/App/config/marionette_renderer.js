﻿Backbone.Marionette.Renderer.render = function (template, data) {
    var path = Handlebars.templates[template];

    if (!path)
        throw "Template fejl. Kunne ikke finde template '" + template + "'."
    return Handlebars.template(path)(data);
};
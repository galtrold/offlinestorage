﻿Vivi.module("Services", function (Services, App, Backbone, Marionette, $, _) {
    Services.defineEndpoint = function(resourceId, _url, _type, parse, map) {
        amplify.request.define(
            resourceId,
            "ajax",
            {
                url: _url,
                dataType: "json",
                type: _type || 'GET',
                contentType: 'application/json; charset=utf-8',
                decoder: parse || {},
                dataMap: map
            }
        );
    }

    Services.Request = function(resourceId, data) {
            var deferred = $.Deferred();
            amplify.request({
                resourceId: resourceId,
                data: data,
                success: deferred.resolve,
                error: deferred.reject
            });
            return deferred;
        
    }
});
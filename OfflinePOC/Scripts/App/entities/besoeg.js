﻿Vivi.module("Entities", function(Entities, App, Backbone, Marionette, $, _) {

    Entities.Besoeg = Entities.Model.extend({
        default: {
            dato: "",
            navn: "",
            kontaktperson: ""
        },
        entityName: "besoeg"
    });

    Entities.BesoegsCollection = Entities.Collection.extend({
        model: Entities.Besoeg,
        entityName: "besoeg",
        initialize: function(options) {
            App.Services.defineEndpoint("besoeg", "/api/besoeg", "GET");
            App.Services.defineEndpoint("createbesoeg", "/api/besoeg", "POST");
            App.Services.defineEndpoint("updatebesoeg", "/api/besoeg/{id}", "UPDATE");
            App.Services.defineEndpoint("deletebesoeg", "/api/besoeg/{id}", "DELETE");
        }
    });


});
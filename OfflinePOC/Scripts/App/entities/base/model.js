﻿Vivi.module("Entities", function (Entities, App, Backbone, Marionette, $, _) {

    Entities.Model = Backbone.Epoxy.Model.extend({
        _lastCleanState: "",
        _forceDirty: false,
        isDirty: function () {
            ate !== this.hashFunction();
        },
        resetDirty: function () {
            this._lastCleanState = this.hashFunction();
        },
        hashFunction: function () {
            var hash = "";
            _.each(this.attributes, function (item) {
                hash += item;
            });
            return hash;
        },
        forceDirty: function() {
            _forceDirty = true;
        },
        constructor: function () {
            Backbone.Model.apply(this, arguments);
            // Check for store name which is required for local storage.
            if (!this.entityName) {
                throw "Entity Model has no 'entityName' which is required for local storage.";
            }
            _.bindAll(this, "createOnline", "createOffline", "updateOnline", "updateOnline", "updateOffline", "deleteOnline", "deleteOffline");

            this._lastCleanState = this.hashFunction();
        },
        sync: function (method, model, options) {
            method = App.localStorageEnabled ? this.getOfflineMethod(method, model) : method;
            console.log("Sync mode: " + method);
            switch (method) {
                case "create":
                    var create = App.localStorageEnabled ? this.createOffline : this.createOnline;
                    create(model, options);
                    break;
                case "read":
                    console.log("Reading model");
                    break;
                case "update":
                    var update = App.localStorageEnabled ? this.updateOffline : this.updateOnline;
                    update(model, options);
                    break;
                case "delete":
                    var destroy = App.localStorageEnabled ? this.deleteOffline : this.deleteOnline;
                    destroy(model, options);
                    break;
            }
        },
        destroy: function (options) {
            options = options ? _.clone(options) : {};
            var model = this;
            var success = options.success;

            var destroy = function () {
                model.trigger('destroy', model, model.collection, options);
            };

            options.success = function (resp) {
                if (options.wait) destroy();
                if (success) success(model, resp, options);
                model.trigger('sync', model, resp, options);
            };

            var error = options.error;
            options.error = function (resp) {
                if (error) error(this, resp, options);
                this.trigger('error', this, resp, options);
            };

            this.garbage(this, options);
            var xhr = this.sync('delete', this, options);
            if (!options.wait) destroy();
            return xhr;
        },

        garbage: function (model, options) {
            if (!model.isNew()) {
                var pendingGarbage = amplify.store("pendingGarbage") || [];
                pendingGarbage.push({ "entityName": model.entityName, "entity": model.toJSON() });
                amplify.store("pendingGarbage", pendingGarbage);
            }
        },
        getOfflineMethod: function (method, model) {
            if (method === "create" || method === "update") {
                return model.has("localId") ? "update" : "create";
            }
            return method;
        },
        createOnline: function (model, options) {
            App.Services.Request("create" + this.entityName, model.toJSON())
                .done(function (result) {
                    options.success(result);
                });
        },
        createOffline: function (model) {
            console.log("Creating model");
            model.set("localId", Entities.guid());
            var besoegArray = amplify.store(this.entityName) || [];
            besoegArray.push(model.toJSON());
            amplify.store(this.entityName, besoegArray);
        },
        updateOnline: function (model) {
            App.Services.Request("update" + this.entityName, model.toJSON())
                .done(function (result) {
                    options.success(result);
                });
        },
        updateOffline: function (model) {
            console.log("Updating model");
            var besoegArray = amplify.store("besoeg") || [];
            var hit = _.find(besoegArray, function (item) {
                return item.localId === model.get("localId");
            });
            _.extend(hit, model.toJSON());
            amplify.store("besoeg", besoegArray);
        },
        deleteOnline: function (model) {

            App.Services.Request("delete" + this.entityName, model.toJSON())
                .done(function (result) {
                    options.success(result);
                });
        },
        deleteOffline: function (model) {
            console.log("Deleting model");
            var besoegArray = amplify.store(this.entityName) || [];
            var hit = _.find(besoegArray, function (item) {
                return item.localId === model.get("localId");
            });
            var index = besoegArray.indexOf(hit);
            besoegArray.splice(index, 1);
            amplify.store(this.entityName, besoegArray);
        }
    },
    {
        showGarbage: function () {
            var pendingGarbage = amplify.store("pendingGarbage");
            _.each(pendingGarbage, function (garbage) {
                console.log(JSON.stringify(garbage, null, 2));
            });
        },
        clearGarbage: function () {
            amplify.store("pendingGarbage", null);
        },
        flushGarbage: function () {
            var pendingGarbage = amplify.store("pendingGarbage");
            _.each(pendingGarbage, function (garbage) {
                App.Services.Request("delete" + garbage.entityName, garbage.model)
                .done(function (result) {
                    var index = pendingGarbage.indexOf(garbage);
                    pendingGarbage.splice(index, 1);
                });
            });
            amplify.store("pendingGarbage", pendingGarbage);
        }
    });

    Entities.guid = (function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                       .toString(16)
                       .substring(1);
        }
        return function () {
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                   s4() + '-' + s4() + s4() + s4();
        };
    })();

});
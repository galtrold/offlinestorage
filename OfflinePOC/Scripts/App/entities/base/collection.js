﻿Vivi.module("Entities", function (Entities, App, Backbone, Marionette, $, _) {

    Entities.Collection = Backbone.Collection.extend({
        constructor: function () {
            // Check for store name which is required for local storage.
            if (!this.entityName) {
                throw "Entity collection has no 'entityName' which is required for local storage.";
            }
            Backbone.Collection.apply(this, arguments);
        },
        sync: function (method, model, options) {
            var localStorageEnabled = App.localStorageEnabled;
            if (method === "read") {
                if (localStorageEnabled) {
                    
                    var besoegArray = amplify.store(this.entityName) || [];
                    options.success(besoegArray);
                    //amplify.store(this.name, null); // Delete store
                } else {
                    App.Services.Request(this.entityName)
                        .done(function (result) {
                            options.success(result);
                        });
                }
            }
        },

    }
    );








});
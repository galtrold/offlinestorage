﻿
Vivi.module("Views", function (Views, App, Backbone, Marionette, $, _) {

    Views.Epoxy = {
        //        bindingHandlers: EpoxyCustomBindingHandlers,
        //        bindingFilters: EpoxyCustomBindingFilters,
        epoxyBindingEnabled: true,
        initialize: function (options) {
            if (this.epoxyBindingEnabled) {
                this.epoxify();
            }
            this.triggerMethod("initialize");
        },

        epoxify: function () {
            Epoxy.View.mixin(this);
            this.listenTo(this, "ui:bind", this.applyBindings);
            this.listenTo(this, "before:close", this.removeBindings);
        },

        // Override Marionette's impl so we can trigger our own event
        bindUIElements: function () {
            this.trigger("ui:bind");
            Marionette.View.prototype.bindUIElements.apply(this, arguments);
        }
    }


});
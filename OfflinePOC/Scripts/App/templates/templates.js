this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};
this["Handlebars"]["templates"]["besogesItemView"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function";


  buffer += "﻿<div class=\"col-xs-3\">\r\n	"
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.dato), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.dato), options)))
    + "\r\n</div>\r\n<div class=\"col-xs-3\">\r\n	";
  if (helper = helpers.navn) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.navn); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\r\n</div>\r\n<div class=\"col-xs-3\">\r\n	";
  if (helper = helpers.kontaktperson) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.kontaktperson); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\r\n</div>\r\n<div class=\"col-xs-3\">\r\n  <button class=\"btn btn-success\" data-role=\"deleteBesoeg\" >Remove</button>\r\n  \r\n  <button class=\"btn btn-success\" data-role=\"editBesoeg\" >Edit</button>\r\n</div>";
  return buffer;
  };
this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};
this["Handlebars"]["templates"]["editBesoegItemView"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function";


  buffer += "﻿<div class=\"col-xs-3\">\r\n	"
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.dato), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.dato), options)))
    + "\r\n</div>\r\n<div class=\"col-xs-3\">\r\n	<div class=\"form-group\">\r\n		<label for=\"navn\">Navn</label>\r\n		<input type=\"text\" class=\"form-control\" id=\"navn\" placeholder=\"Navn\" value=\"";
  if (helper = helpers.navn) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.navn); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\" />\r\n    \r\n  </div>\r\n</div>\r\n<div class=\"col-xs-3\">\r\n	<div class=\"form-group\">\r\n			<label for=\"kontakt\">Email address</label>\r\n			<input type=\"text\" class=\"form-control\" id=\"kontakt\" placeholder=\"Kontaktperson\" />\r\n	  </div>	\r\n</div>\r\n<div class=\"col-xs-3\">\r\n  <button class=\"btn btn-success\" data-role=\"editBesoeg\">Luk</button>\r\n</div>";
  return buffer;
  };
this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};
this["Handlebars"]["templates"]["forside"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "﻿<div>\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-9\">\r\n      <h2>\r\n        Besoegs liste\r\n      </h2>\r\n    </div>\r\n    <div class=\"col-xs-3\">\r\n      <button class=\"btn btn-info\">Sync</button>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\" id=\"besoegsListe\">\r\n    <div class=\"col-xs-12\">\r\n      <button class=\"btn btn-success addBesoegBtn\">Tilføj besøg</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n";
  };